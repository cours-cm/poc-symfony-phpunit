# POC SYMFONY/PHP UNIT

## ROADMAP

### INSTALLATION

Pour pouvoir utiliser ce POC, il faut installer les dépendances du projet avec la commande suivante :

```bash
composer install
```

### PREPARATION DU PROJET

#### 08/03/2023

* Mise en place du projet et du git
* Paramètrage dans le .env de l'application de la base de données de l'application et de la base de données de test
  * php bin/console doctrine:database:create
  * php bin/console doctrine:database:create --env=test
* Mise en place d'un entité Product (migration & migrate)
* Mise en place du schema de la base de données dans la base de données de test
  * php bin/console doctrine:schema:create --env=test
* Mise en place de la fixture ProductFixtures (et mise en place de références pour les produits, au nombre de deux dans ce POC)
* Chargement des fixtures dans la base de données
* Mise en place du CRUD de l'entité Product (avec validation de la prise en charge des tests unitaires)
* Mise en commentaire dans les quatre tests unitaires de la classe ProductControllerTest de la méthode testShowProduct() pour pouvoir lancer les tests unitaires

## HOWTO

### PREMIERS TESTS UNITAIRES

Pour utiliser ce poc, il faut maintenant lancer des commandes dans le terminal :

```bash
# Lancer les tests unitaires
php bin/phpunit
````

On obtient alors le résultat suivant :

![Premiers résultats](/public/images/readme-image1.png "Premiers résultats")

Ok les tests unitaires passent, mais on ne voit pas grand chose. Pour améliorer le retour des tests unitaires, on peut utiliser les options suivantes :

```bash
# Lancer les tests unitairesun rapport dans le terminal
php bin/phpunit --verbose
````

Avec cette commande, les tests échoués sont affichés dans le terminal.

---

```bash
# Lancer les tests unitaires avec le rapport de couverture
php bin/phpunit --coverage-html tests/Reports/html
````

Avec cette commande, un dossier recevant le rapport de couverture est créé dans le dossier tests/Reports/html. On peut alors ouvrir le fichier index.html dans le navigateur pour voir le rapport de couverture.

---

### TESTS PARTIELS

Réaliser des tests partiels il suffit