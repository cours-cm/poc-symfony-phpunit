<?php

namespace App\Test\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private ProductRepository $repository;
    private string $path = '/product/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get('doctrine')->getRepository(Product::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Product index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        // On commente la ligne suivante pour que le test passe sinon il sera marqué comme incomplet dans la console
        // $this->markTestIncomplete();

        $this->client->request('POST', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'product[name]' => 'Testing',
            'product[description]' => 'Testing',
        ]);

        self::assertResponseRedirects('/product/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    // public function testShow(): void
    // {
    //     // On commente la ligne suivante pour que le test passe sinon il sera marqué comme incomplet dans la console
    //     // $this->markTestIncomplete();
    //     $fixture = new Product();
    //     $fixture->setName('My Title');
    //     $fixture->setDescription('My Title');

    //     $this->repository->save($fixture, true);

    //     $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

    //     self::assertResponseStatusCodeSame(200);
    //     self::assertPageTitleContains('Product');

    //     // Use assertions to check that the properties are properly displayed.
    // }

    // public function testEdit(): void
    // {
    //     // On commente la ligne suivante pour que le test passe sinon il sera marqué comme incomplet dans la console
    //     $this->markTestIncomplete();
    //     $fixture = new Product();
    //     $fixture->setName('My Title');
    //     $fixture->setDescription('My Title');

    //     $this->repository->save($fixture, true);

    //     $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

    //     $this->client->submitForm('Update', [
    //         'product[name]' => 'Something New',
    //         'product[description]' => 'Something New',
    //     ]);

    //     self::assertResponseRedirects('/product/');

    //     $fixture = $this->repository->findAll();

    //     self::assertSame('Something New', $fixture[0]->getName());
    //     self::assertSame('Something New', $fixture[0]->getDescription());
    // }

    // public function testRemove(): void
    // {
    //     // On commente la ligne suivante pour que le test passe sinon il sera marqué comme incomplet dans la console
    //     // $this->markTestIncomplete();

    //     $originalNumObjectsInRepository = count($this->repository->findAll());

    //     $fixture = new Product();
    //     $fixture->setName('My Title');
    //     $fixture->setDescription('My Title');

    //     $this->repository->save($fixture, true);

    //     self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

    //     $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
    //     $this->client->submitForm('Delete');

    //     self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
    //     self::assertResponseRedirects('/product/');
    // }
}
