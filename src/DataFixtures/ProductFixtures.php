<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ProductFixtures extends Fixture
{
    // ====================================================== //
    // ===================== PROPRIETES ===================== //
    // ====================================================== //
    public const PRODUCT_REFERENCE_1 = 'product1';
    public const PRODUCT_REFERENCE_2 = 'product2';
    // ====================================================== //
    // ====================== METHODES ====================== //
    // ====================================================== //
    public function load(ObjectManager $manager): void
    {
        $product = new Product();
        $product->setName('Keyboard');
        $product->setDescription('Ergonomic and stylish keyboard!');
        $manager->persist($product);
        $this->addReference(self::PRODUCT_REFERENCE_1, $product);

        $product = new Product();
        $product->setName('Mouse');
        $product->setDescription('Ergonomic and stylish mouse!');
        $manager->persist($product);
        $this->addReference(self::PRODUCT_REFERENCE_2, $product);

        $manager->flush();
    }
}
